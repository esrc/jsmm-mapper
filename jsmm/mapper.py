"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import eaccpf
import geopy
import logging
import os
import shutil

from lxml import etree

geocoder = geopy.geocoders.GoogleV3()
source = '/srv/ha/web/JSMM/LIVE/eac'
output = '/var/lib/indexer/JSMM'
locations = {}

# configure logging
log_format = '%(asctime)s - %(filename)s %(lineno)03d - %(levelname)s - %(message)s'
formatter = logging.Formatter(log_format)
sh = logging.StreamHandler()
sh.setFormatter(formatter)
sh.setLevel(logging.INFO)

log = logging.getLogger(__name__)
log.addHandler(sh)


def getAddressParts(Address):
    """
    Parse a location record or address string into components.
    """
    street = city = region = postal = country = ''
    try:
        # split the address string into segments
        segments = Address.split(',')
        country = segments.pop().strip()
        # a token is a postal code if it has numbers in it
        tokens = segments.pop().strip().split(' ')
        for token in reversed(tokens):
            if any(c.isdigit() for c in token):
                postal = token + ' ' + postal
                tokens.pop()
        postal = str(postal.strip())
        # the next token should be the region and the remainder
        # should be the city
        for token in reversed(tokens):
            if region == '':
                region = tokens.pop().strip()
            elif city == '':
                city = ' '.join(tokens).strip()
        if segments and city == '':
            city = segments.pop().strip()
        # the remaining segments should be part of the street
        # address
        if segments:
            street = ','.join(segments).strip()
    except:
        pass
    return street, city, region, postal, country

def infer_location(PlaceName):
    # if we've already cached the placename location,
    # then use the cached value
    if locations.has_key(PlaceName) and locations[PlaceName] != None:
        return locations[PlaceName]
    # look up the location
    location = {}
    try:
        address, (lat, lng) = geocoder.geocode(PlaceName, exactly_one=True)
        location['address'] = address
        location['latitude'] = lat
        location['longitude'] = lng
        location['lat_long'] = [lat, lng]
        street, city, region, postal_code, country = getAddressParts(address)
        location['country'] = country
        location['postal_code'] = postal_code
        location['region'] = region
        location['city'] = city
        location['street'] = street
    except:
        pass
    return location

def process_files(source, output):
    # create the output folder, or clean it out if it
    # already exists
    try:
        shutil.rmtree(output)
    except:
        pass
    os.mkdir(output)
    # process each input file
    files = [filename for filename in os.listdir(source) if filename.endswith('.xml')]
    for filename in files:
        path = source + os.sep + filename
        try:
            eac = eaccpf.Document(Source=path, PresentationUrl="http://web.esrc.unimelb.edu.au/JSMMt/{0}".format(filename))
            # create a solr input document with metadata
            root = etree.Element("add")
            doc = etree.SubElement(root, "doc")
            # document id
            f = etree.SubElement(doc, "field")
            f.attrib['name'] = 'id'
            f.text = eac.getEntityId()
            # title
            f = etree.SubElement(doc, "field")
            f.attrib['name'] = 'title'
            f.text = eac.getTitle()
            # local type
            f = etree.SubElement(doc, "field")
            f.attrib['name'] = 'localtype'
            f.text = eac.getLocalType()
            # for each place, add the location information and
            # write a Solr Input Document
            for place in eac.getPlaces():
                # place role
                f = etree.SubElement(doc, "field")
                f.attrib['name'] = 'place_role'
                f.text = place['place_role']
                # place entry
                f = etree.SubElement(doc, "field")
                f.attrib['name'] = 'place_entry'
                f.text = place['place_entry']
                # get location data
                location = infer_location(place['place_entry'])
                # latitude, longitude
                lat = str(location['latitude'])
                lng = str(location['longitude'])
                f = etree.SubElement(doc, "field")
                f.attrib["name"] = "location"
                f.text = "{0}, {1}".format(lat, lng)
                # latitude
                f = etree.SubElement(doc, "field")
                f.attrib["name"] = "location_0_coordinate"
                f.text = lat
                # longitude
                f = etree.SubElement(doc, "field")
                f.attrib["name"] = "location_1_coordinate"
                f.text = lng
                # write the solr input document
                fn = "{0}-{1}.xml".format(eac.getRecordId(), place['place_role'].lower().replace(" ","_"))
                with open(output + os.sep + fn, 'w') as f:
                    xml = etree.tostring(root, pretty_print=True)
                    f.write(xml)
                    f.close()
        except:
            log.error("Could not complete processing on {0}".format(filename), exc_info=True)


if __name__ == "__main__":
    process_files(source, output)
