JSMM Data Mapper
================

Utility to map data elements from the JSMM data set.


Credits
-------

JSMM Data Mapper is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 > eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>

Thanks:

 * GeoPy - http://code.google.com/p/geopy
 * Google Maps API - http://maps.google.com
 * lxml - http://lxml.de
 * Pairtree - https://pypi.python.org/pypi/Pairtree
 * Python - http://www.python.org


License
-------

Please see the LICENSE file for license information.


Usage
-----

Run python mapper.py -h for usage instructions and a list of options.


Revision History
----------------
